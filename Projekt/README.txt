https://www.tutorialspoint.com/html/
https://www.tutorialspoint.com/css/
https://www.tutorialspoint.com/javascript/

sudo systemctl mysql start

mysqladmin -p -u root version

mysql -u root -p #MYSQL CLIENT

/etc/init.d/mysqld start // Boot TIME

ps -ef | grep mysqld // running mysql server

sudo /etc/init.d/mysql start      // START Server

cd /usr/bin
./mysqladmin -u root -p shutdown      // SHUTDOWN Server

OPIS BAZY DANYCH - OLIMPIADA SPORTOWA

WYKONAWCA - DAMIAN STACHURA

Plik data.sql zawiera przyk�adowe dane wstawione do poni�szych tabel.

OPIS RELACJI :

1) NARODY - zawiera nazwy kraj�w uczestnicz�cych w olimpiadze oraz numer budynku, w kt�rym mieszka reprezentacja w wiosce olimpijskiej(klucz obcy)

2) ZAWODNICY - zawiera dane osobowe zawodnika, kraj (klucz obcy na narody), liczbe wyst�p�w na olimpiadzie, konkurencje (klucz obcy na dyscypliny), zespol(informacja typu integer m�wi, w kt�rym zespole jest zawodnik, a je�li w �adnym to w polu jest cyfra 0), kolumna indywidualnie(typu char - 'T' oznacza, �e zawodnik startuje indywidualnie, 'N' - nie zosta� zg�oszony indywidualnie)

3) ZESPOLY - zawiera id_zespolu (klucz podstawowy), kraj kt�ry dany zesp�� reprezentuje, dyscypline w kt�rej wyst�puje, p�e� zespo�u oraz typ_rozgrywki, kt�ry trzyma informacje czy wyniki zespo�u prezentowane s� jako wynik pewnego meczu z innym zespo�em ('M') czy jest to wynik niezale�ny od innych zespo��w, przyk�adowo sztafeta ('I')

4) DYSCYPLINY - id_konkurencji (klucz podstawowy), dyscyplina(nazwa), arena - miejsce w ktrym rozgrywane s� zmagania w danej dyscyplinie, typ (trzyma informacje na temat czy dyscyplina jest indywidualna('I') czy dru�ynowa('Z'))

5) ARENY - nr_areny(klucz podstawowy), nazwa, rok_otwarcia (rok sko�czenia budowy danej areny),
pojemno�� (ilo�� miejsc dla kibic�w), strefa (zbi�r aren do kt�rego nale�y dana arena)

6) STREFY - id_stefy oraz jej nazwa, miasto w kt�rym znajduje si� strefa

7) REKORDY - zawiera rekordy �wiata i olimpijskie w ka�dej z dyscyplin indywidualnych oraz
pole dyscyplina(klucz obcy na tabele dyscypliny)

8) S�DZIOWIE - id_sedziego, dane(imie, nazwisko oraz kraj z kt�rego jest), dyscyplina(klucz obcy na dyscypliny)

9) WIOSKA_OLIMPIJSKA - zawiera zbi�r budynk�w, w kt�rych mieszkaj� reprezentacje. ocena_budynku (nota w skali od 1-5)

10) SPIS_ZGLOSZEN - tabela zawiera wszystkie zg�oszenia zawodnik�w do dyscyplin w kt�rych wyst�puj�. Przyk�adowo jeden zawodnik, mo�e si� w tej tabeli znale�� kilkukrotnie, je�li wyst�puje w wi�cej ni� jednej dyscyplinie

11) WYNIKI - zawiera wyniki ka�dego zg�oszenia, kt�rego wyniki s� reprezentowanie indywidualnie dla ka�dego zawodnika. To znaczy ostateczny wynik, jednostke w kt�rej zosta� podany wynik(czas ('C'), odleg�o��('O')) lub punkty('P')). Pole podwyniki trzyma informacje o tym czy w danym wyniku wyst�puj� podwyniki('T' lub 'N').

12) PODWYNIKI_IND - Podwyniki dyscyplin indywidualnych reprezentowane jako string.

13) WYNIKI_IND_MECZE - wyniki dyscyplin indywidualnych, w kt�rych rodzajem rozgrywki s� mecze. Tabela zawiera id dw�ch zespo��w oraz oraz informacje o wyniku (0 - remis, 1 - wygrana

14) PODWYNIKI2 - podwyniki powy�szej tabeli

15) WYNIKI_DRUZ_INNE - wyniki dyscyplin dru�ynowych, gdzie rodzajem rozgrywki nie s� mecze. Tabela zawiera wynik danego zespo�u oraz jednostk�, w kt�rej wynik zosta� uzyskany (punkty, czas b�d� odleg�o��).

16) WYNIKI_MECZE - wyniki dru�yn reprezentowane jako mecze, podobnie jak tabela wyniki_ind_mecze

17) PODWYNIKI - zawiera podwyniki wszystkich mecz�w, przyk�adowo w meczu siatk�wki podwynikami s� kolejne sety. Podwyniki s� przedstawione w stringu.
