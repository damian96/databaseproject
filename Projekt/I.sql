--ZAD9
CREATE OR REPLACE RULE auto_delete AS
ON DELETE TO pacjenci
DO
DELETE FROM wizyty WHERE pacjent = OLD.pesel;
----
--ZAD8
CREATE OR REPLACE RULE non_delete AS
ON DELETE TO lekarze
WHERE OLD.id IN (SELECT id
FROM lekarze l JOIN specjalizacje s ON l.id = s.id_lekarza
WHERE specjalizacja = 'Chirurgia'
)
DO INSTEAD NOTHING;
----

--ZAD7
CREATE OR REPLACE VIEW chirurdzy("id", "imie", "nazwisko") AS
 SELECT id, imie, nazwisko
 FROM lekarze l join specjalizacje s on l.id = s.id_lekarza
 WHERE specjalizacja = 'Chirurgia';

 CREATE OR REPLACE RULE chirurdzy_protect AS
    ON DELETE TO chirurdzy
    DO INSTEAD NOTHING;
----

--ZAD6
CREATE OR REPLACE VIEW pediatrzy("id", "imie", "nazwisko") AS
 SELECT id, imie, nazwisko
 FROM lekarze l join specjalizacje s on l.id = s.id_lekarza
 WHERE specjalizacja = 'Pediatria';

 CREATE OR REPLACE RULE pediatrzy_protect AS
    ON INSERT TO pediatrzy
    DO INSTEAD
    INSERT INTO lekarze(id, imie, nazwisko) VALUES (NEW.id, NEW.imie, NEW.nazwisko);

 CREATE OR REPLACE RULE pediatrzy_protect1 AS
    ON INSERT TO pediatrzy
    DO INSTEAD
    INSERT INTO specjalizacje(id_lekarza, specjalizacja) VALUES (NEW.id, 'Pediatria');
----
--ZAD5
CREATE TABLE lekarze_prowadzacy(
pesel char(11) NOT NULL,
lekarz integer NOT NULL
);

ALTER TABLE ONLY lekarze_prowadzacy
    ADD CONSTRAINT pk_lekarze_prowadzacy PRIMARY KEY (pesel, lekarz);

ALTER TABLE ONLY lekarze_prowadzacy
    ADD CONSTRAINT fk_prowadzacy_lekarze FOREIGN KEY (lekarz) REFERENCES lekarze(id);

ALTER TABLE ONLY lekarze_prowadzacy
    ADD CONSTRAINT fk_prowadzacy_pacjenci FOREIGN KEY (pesel) REFERENCES pacjenci(pesel);

CREATE OR REPLACE FUNCTION cos_tam() RETURNS trigger
AS $$
DECLARE
wynik integer;
BEGIN
select id_lekarza
into wynik
from lekarze
left join lekarze_prowadzacy l on id = l.lekarz
left join specjalizacje on id = id_lekarza
where specjalizacja = 'Medycyna rodzinna'
group by id_lekarza
order by count(lekarz), id_lekarza
LIMIT 1;

INSERT INTO lekarze_prowadzacy(pesel, lekarz) VALUES (NEW.pesel, wynik);
RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER cos_tam AFTER INSERT ON pacjenci
FOR EACH ROW EXECUTE PROCEDURE cos_tam();
----
--ZAD4
CREATE OR REPLACE FUNCTION podstawowa() RETURNS TRIGGER AS $$
BEGIN
    IF NEW.data_zakonczenia-NEW.data_rozpoczecia>(INTERVAL '60 minutes')
        OR (SELECT count (*) FROM wizyty WHERE
            lekarz=NEW.lekarz AND
            NEW.data_rozpoczecia<data_zakonczenia  AND data_rozpoczecia<NEW.data_zakonczenia
            )>0
        THEN
        RETURN NULL;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER podstawowa BEFORE INSERT ON wizyty
FOR EACH ROW EXECUTE PROCEDURE podstawowa();

---


--ZAD3
CREATE OR REPLACE FUNCTION dropvisit()
RETURNS trigger AS $$
BEGIN
IF now() - OLD.data_rozpoczecia < INTERVAL '5 years' THEN
RETURN NULL;
END IF;
RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER dropvisit
BEFORE DELETE ON wizyty
FOR EACH ROW EXECUTE PROCEDURE dropvisit();
----

--ZAD2
CREATE OR REPLACE FUNCTION check_visit() RETURNS TRIGGER AS $$
BEGIN
IF NEW.data_zakonczenia IS NULL
THEN NEW.data_zakonczenia = NEW.data_rozpoczecia + interval '30 minutes';
END IF;
RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_visit
BEFORE INSERT ON wizyty
FOR EACH ROW EXECUTE PROCEDURE check_visit();
----

--ZAD1
CREATE OR REPLACE FUNCTION pesel_check() RETURNS TRIGGER AS $$
DECLARE
arr int[];
tmp int;
BEGIN
  IF char_length(NEW.pesel) != 11 THEN RAISE EXCEPTION 'Niepoprawny PESEL';
  END IF;
  arr = regexp_split_to_array(NEW.pesel, '');
  tmp = (1*arr[1] + 3*arr[2] + 7*arr[3] + 9*arr[4] + 1*arr[5] + 3*arr[6] + 7*arr[7] + 9*arr[8] + 1*arr[9] + 3*arr[10])%10;
  IF --(char_length(NEW.pesel) != 11) OR
  NEW.pesel is null OR
  (10 - tmp)%10 != arr[11]
  THEN RAISE EXCEPTION 'Niepoprawny PESEL';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER pesel_check
BEFORE INSERT OR UPDATE ON pacjenci
FOR EACH ROW EXECUTE PROCEDURE pesel_check();
----
