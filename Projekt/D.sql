--ZAD9
select produkty.nazwa, round(cena_netto*(1+(VAT/100)),2)
from produkty
join historia_cen z using(kod_produktu)
join kategorie using(id_kategoria)
where data_wprowadzenia >= ALL(
select data_wprowadzenia
from historia_cen
where kod_produktu = z.kod_produktu
);
----
--ZAD7
select nazwa, count(data_wprowadzenia)
from historia_cen k left outer join produkty using(kod_produktu)
where extract(month from data_wprowadzenia)=4
group by kod_produktu, nazwa
having 2 <=(
select count(data_wprowadzenia)
from historia_cen z
where extract(month from data_wprowadzenia)=4 AND k.kod_produktu = z.kod_produktu
group by kod_produktu
);
----
--ZAD6
select id_zamowienia
from produkty_zamowienia
group by id_zamowienia
having sum(ilosc)>= ALL(
select sum(ilosc)
from produkty_zamowienia
group by id_zamowienia);
----
--ZAD5
SELECT k.*
FROM kategorie k
WHERE vat < ANY(select vat
FROM kategorie
where k.id_kategoria = nadkategoria
group by vat
);
----
--ZAD4
select rabaty.*
from rabaty
where age(data_do, data_od) = (
select max(age(data_do, data_od))
from rabaty);
----
--ZAD3c
select kategorie.nazwa
from produkty join kategorie using(id_kategoria)
group by kategorie.id_kategoria, kategorie.nazwa
having count(produkty.nazwa)>2;
----
--ZAD3a
select kategorie.nazwa
from produkty join kategorie using(id_kategoria)
where kategorie.id_kategoria IN
(select id_kategoria
from produkty
group by id_kategoria
having count(nazwa)>2)
group by kategorie.id_kategoria, kategorie.nazwa;
----
--ZAD2
select nazwa
from produkty
where id_kategoria IN
(
select id_kategoria
from kategorie
where nadkategoria is not null
group by id_kategoria
);
----
--ZAD1
select nazwa
from produkty
where id_kategoria = (select id_kategoria from produkty where nazwa = 'Piórnik duży');
----
