--ZAD17
select a.id_zamowienia, max(ilosc)
from zamowienia a
join produkty_zamowienia b using(id_zamowienia)
group by a.id_zamowienia
order by max DESC, a.id_zamowienia;
----
--ZAD16
select
(CASE
when count(*)>0 then 'TAK'
else 'NIE'
END)
from rabaty_klientow a
join rabaty_klientow b
on a.id_polecajacego = b.id_klienta AND a.id_klienta = b.id_polecajacego;
----
--ZAD15
select a.*,
(CASE
when a.id_rabatu = b.id_rabatu then 'RABAT KLIENCKI'
when a.id_rabatu = c.id_rabatu then 'RABAT PRODUKTU'
END) as "typ rabatu"
from rabaty a
left outer join rabaty_klientow b using (id_rabatu)
left outer join rabaty_produktow c using (id_rabatu)
group by a.id_rabatu, b.id_rabatu, c.id_rabatu
order by a.id_rabatu;
----
--ZAD14
select klienci.nazwa as klient, (
CASE
when count(id_zamowienia)>0 then count(id_zamowienia)
else 0
END) as "ilosc zamowien"
from klienci
left outer join zamowienia using(id_klienta)
group by klienci.id_klienta, klient
order by "ilosc zamowien" DESC, klient;
----
--ZAD13
select a.nazwa as klient,(
CASE
when count(kod_produktu) is not null then sum(ilosc)
else 0
END) as "ilosc produktow"
from klienci a
left outer join zamowienia b on a.id_klienta = b.id_klienta
left outer join produkty_zamowienia c on b.id_zamowienia = c.id_zamowienia
left outer join produkty d on produkt=kod_produktu
where waga>1000
group by a.id_klienta
order by klient, "ilosc produktow";
----

--ZAD12
select a.nazwa as klient,
(CASE
when b.id_klienta is not null then sum(ilosc)
else 0
END) as "ilosc produktow"
from klienci a
left outer join zamowienia b on a.id_klienta = b.id_klienta
left outer join produkty_zamowienia c on b.id_zamowienia = c.id_zamowienia
group by a.id_klienta, b.id_klienta
order by klient, "ilosc produktow";
----
--ZAD10
select klienci.*, zamowienia.id_zamowienia, sum(ilosc*cena_netto)
from klienci
join zamowienia on klienci.id_klienta = zamowienia.id_klienta
join produkty_zamowienia on zamowienia.id_zamowienia = produkty_zamowienia.id_zamowienia
join produkty on produkt = kod_produktu
where klienci.nazwa = 'Urząd Skarbowy w Radomiu'
group by zamowienia.id_zamowienia, klienci.id_klienta
order by sum DESC, zamowienia.id_zamowienia;
----
--ZAD9
select nazwa
from produkty
join produkty_zamowienia a on kod_produktu = produkt
join rabaty_produktow b on a.produkt = b.produkt
where min_ilosc = 1
group by a.produkt, nazwa
order by nazwa;
----
--ZAD8
select distinct a.id_klienta, nazwa, numer_telefonu, email, nip
from klienci a left outer join rabaty_klientow b on
a.id_klienta = b.id_klienta
where b.id_klienta is null
order by id_klienta, nazwa, numer_telefonu, email, nip;
----
--ZAD7b
select a.nazwa, b.nazwa
from klienci a
join rabaty_klientow c on a.id_klienta = c.id_klienta
left outer join klienci b on c.id_polecajacego = b.id_klienta
where id_rabatu is not null
order by a.nazwa, b.nazwa;
----
--ZAD7a
select a.nazwa, b.nazwa
from klienci a
join rabaty_klientow c on a.id_klienta = c.id_klienta
join klienci b on c.id_polecajacego = b.id_klienta
where id_rabatu is not null AND id_polecajacego is not null
order by a.nazwa, b.nazwa;
----
--ZAD6
select nazwa, email
from klienci join zamowienia using(id_klienta)
where EXTRACT(MONTH FROM data_zlozenia) = 3
group by id_klienta
order by nazwa, email;
----
--ZAD5
select a.nazwa, b.nazwa
from produkty as a
join produkty as b
on a.kod_produktu != b.kod_produktu
where a.cena_netto*(1+(a.vat/100)) > b.cena_netto*(1+(b.vat/100))
order by a.nazwa, b.nazwa;
----
--ZAD4
select nazwa as "nazwa klienta",
(case
when count(distinct adres_dostawy)>1 then 'TAK'
else 'NIE'
end) AS "rozne adresy"
from klienci join zamowienia using(id_klienta)
group by id_klienta
order by "nazwa klienta", "rozne adresy";
----
--ZAD3
select distinct produkty.nazwa
from zamowienia z
join produkty_zamowienia pz on z.id_zamowienia = pz.id_zamowienia
join produkty on pz.produkt = produkty.kod_produktu
where adres_dostawy LIKE 'ul. Kubusia Puchatka%'
order by produkty.nazwa;
----
--ZAD2
select data_zlozenia, nazwa
from zamowienia join klienci on
zamowienia.id_klienta = klienci.id_klienta
where nip is not null
order by data_zlozenia, nazwa;
----
--ZAD1
select produkty.nazwa, klienci.nazwa
from produkty cross join klienci
where kategoria like 'Artykuły%' and email IS NOT NULL
order by produkty.nazwa, klienci.nazwa;
----
