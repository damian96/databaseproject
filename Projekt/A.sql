--ZAD22
SELECT IMIE, NAZWISKO, PLACA_DOD FROM PRACOWNICY
ORDER BY PLACA_DOD DESC NULLS LAST, ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY, PLACA_POD, ID_ZESP;
----
--ZAD21
SELECT ID_PRAC,
CASE
WHEN PLACA_POD<1850 then 'mniej'
WHEN PLACA_POD=1850 then 'rowna'
WHEN PLACA_POD>1850 then 'wiecej'
END AS PLACA
FROM PRACOWNICY
ORDER BY ID_PRAC, PLACA;
----
--ZAD19
SELECT NAZWISKO, TO_CHAR(ZATRUDNIONY, 'Month, DD YYYY') AS DATA_ZATRUDNIENIA FROM PRACOWNICY WHERE ID_ZESP=20
ORDER BY ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY, PLACA_POD, PLACA_DOD, ID_ZESP;
----
--ZAD18
SELECT NAZWISKO, TRANSLATE(NAZWISKO, 'KkLlMm', 'XXXXXX') AS ZMIENIONE_NAZWISKO FROM PRACOWNICY ORDER BY NAZWISKO, ZMIENIONE_NAZWISKO;
----
--ZAD17
SELECT SUBSTRING(ETAT FROM 1 FOR 2) || ID_PRAC , ID_PRAC, NAZWISKO, ETAT FROM PRACOWNICY
ORDER BY SUBSTRING(ETAT FROM 1 FOR 2) || ID_PRAC, ID_PRAC, NAZWISKO, ETAT;
----
--ZAD16
select rpad(nazwisko, (40-char_length(etat)), '.')  || etat as "nazwisko i etat"
from pracownicy
order by ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY, PLACA_POD, PLACA_DOD, ID_ZESP;
----
--ZAD15
SELECT NAZWISKO, ROUND(PLACA_POD+(0.15*PLACA_POD),0) AS PLACA_POD FROM PRACOWNICY ORDER BY NAZWISKO, PLACA_POD;
----
--ZAD14
SELECT NAZWISKO || ' pracuje od ' ||  ZATRUDNIONY || ' i zarabia ' || PLACA_POD FROM PRACOWNICY WHERE ETAT='PROFESOR'
ORDER BY PLACA_POD DESC, NAZWISKO, ZATRUDNIONY;
----
--ZAD13
SELECT * FROM PRACOWNICY
WHERE PLACA_DOD>500
ORDER BY ETAT, NAZWISKO, ID_PRAC, IMIE, ID_SZEFA, ZATRUDNIONY, PLACA_POD, PLACA_DOD, ID_ZESP;
----
--ZAD12
SELECT NAZWISKO, ZATRUDNIONY, ETAT FROM PRACOWNICY
WHERE ZATRUDNIONY >= '1992-01-01' AND ZATRUDNIONY <= '1993-12-31'
ORDER BY NAZWISKO, ETAT, ZATRUDNIONY;
----
--ZAD11
SELECT * FROM PRACOWNICY
WHERE PLACA_POD > 2000 AND ID_SZEFA IS NOT NULL
ORDER BY ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY, PLACA_POD, PLACA_DOD, ID_ZESP;
----
--ZAD10
SELECT * FROM PRACOWNICY
WHERE NAZWISKO LIKE '%ski'
ORDER BY ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY, PLACA_POD, PLACA_DOD, ID_ZESP;
----
--ZAD8
SELECT * FROM PRACOWNICY WHERE ID_ZESP='20' OR ID_ZESP='30'
ORDER BY PLACA_POD DESC, ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY,
PLACA_DOD, ID_ZESP;
----
--ZAD9
SELECT * FROM PRACOWNICY
WHERE PLACA_POD BETWEEN 300 AND 1800
ORDER BY ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY, PLACA_POD, PLACA_DOD, ID_ZESP;
----
--ZAD7
SELECT *
   FROM PRACOWNICY
   WHERE ETAT = 'ASYSTENT';
----
--ZAD6
SELECT DISTINCT ETAT FROM PRACOWNICY;
----
--ZAD5
SELECT * FROM ZESPOLY
    ORDER BY NAZWA;
----
--ZAD4
SELECT NAZWISKO, PLACA_POD + COALESCE(PLACA_DOD,0) FROM PRACOWNICY
ORDER BY NAZWISKO, (PLACA_POD + PLACA_DOD);
----
--ZAD3
SELECT NAZWISKO, ETAT, 12*PLACA_POD FROM PRACOWNICY
 ORDER BY NAZWISKO, ETAT, PLACA_POD;
----
--ZAD2
SELECT * FROM PRACOWNICY
    ORDER BY ID_PRAC, NAZWISKO, IMIE, ETAT, ID_SZEFA, ZATRUDNIONY, PLACA_POD, PLACA_DOD, ID_ZESP;
----
--ZAD1
SELECT * FROM ZESPOLY
ORDER BY ID_ZESP, NAZWA, ADRES;
----
