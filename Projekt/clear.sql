ALTER TABLE podwyniki DROP CONSTRAINT fk_podwyniki_wyniki_mecze; 

ALTER TABLE wyniki_mecze DROP CONSTRAINT fk_wyniki_mecze_zespoly;

ALTER TABLE wyniki_mecze DROP CONSTRAINT fk_wyniki_mecze_zespoly2;

ALTER TABLE rekordy DROP CONSTRAINT fk_rekordy_swiata;

ALTER TABLE s�dziowie DROP CONSTRAINT fk_s�dziowie;

ALTER TABLE podwyniki2 DROP CONSTRAINT fk_podwyniki2_ind_mecze;

ALTER TABLE wyniki_ind_mecze DROP CONSTRAINT fk_wyniki_mecze_zgloszenia;

ALTER TABLE wyniki_ind_mecze DROP CONSTRAINT fk_wyniki_mecze_zgloszenia_2; 

ALTER TABLE podwyniki_ind DROP CONSTRAINT fk_podwyniki_ind_wyniki;

ALTER TABLE wyniki DROP CONSTRAINT fk_wyniki_spis_zgloszen;

ALTER TABLE wyniki_druz_inne DROP CONSTRAINT fk_wyniki_druz_inne_zespoly; 

ALTER TABLE spis_zgloszen DROP CONSTRAINT fk_spis_zgloszen_dyscypliny;

ALTER TABLE spis_zgloszen DROP CONSTRAINT fk_spis_zgloszen_zawodnicy;

ALTER TABLE zawodnicy DROP CONSTRAINT fk_zawodnicy_narody;

ALTER TABLE zespoly DROP CONSTRAINT fkey_zespoly_narody;

ALTER TABLE zespoly DROP CONSTRAINT fk_zespoly_dyscypliny;

ALTER TABLE dyscypliny DROP CONSTRAINT fk_dyscypliny_indywidualne;

ALTER TABLE areny DROP CONSTRAINT fk_areny_strefy;

ALTER TABLE narody DROP CONSTRAINT fk_narody_wioska_olimpijska;

DROP TABLE wioska_olimpijska CASCADE;
DROP TABLE narody CASCADE;
DROP TABLE strefy CASCADE;
DROP TABLE areny CASCADE;
DROP TABLE dyscypliny CASCADE;
DROP TABLE zespoly CASCADE;
DROP TABLE zawodnicy CASCADE;
DROP TABLE spis_zgloszen CASCADE;
DROP TABLE wyniki_druz_inne CASCADE;
DROP TABLE wyniki CASCADE;
DROP TABLE podwyniki_ind CASCADE;
DROP TABLE wyniki_ind_mecze CASCADE;
DROP TABLE podwyniki2 CASCADE;
DROP TABLE s�dziowie CASCADE;
DROP TABLE rekordy CASCADE;
DROP TABLE wyniki_mecze CASCADE;
DROP TABLE podwyniki CASCADE;