--ZAD14
CREATE FUNCTION moment_rozspojniajacy() RETURNS timestamp without time zone
AS $$
DECLARE
r transakcje%rowtype;
min_data timestamp without time zone;
arr numeric(11, 2)[];
max numeric;
 BEGIN
   max = (SELECT max(nr_konta) FROM konta);
   FOR i IN 1..max LOOP
   arr[i] = 0.00;
   END LOOP;
   FOR r IN
      SELECT * FROM transakcje ORDER BY data_zlecenia
   LOOP
      IF r.z_konta is not null THEN arr[r.z_konta] = arr[r.z_konta] - r.kwota;
        END IF;
      IF r.na_konto is not null THEN arr[r.na_konto] = arr[r.na_konto] + r.kwota;
      END IF;
      IF arr[r.na_konto] < 0.00 OR arr[r.z_konta] < 0.00
        THEN RETURN r.data_zlecenia;
      END IF;
   END LOOP;
   RETURN NULL;
 END;
$$ language plpgsql;
----

--ZAD13
CREATE OR REPLACE FUNCTION stan_konta(konto numeric, czas timestamp) RETURNS numeric(11, 2) AS
$$
DECLARE
wynik numeric(11, 2);
r transakcje%rowtype;
 BEGIN
   wynik = 0.00;
   FOR r IN
      SELECT * FROM transakcje WHERE data_zlecenia <= czas ORDER BY data_zlecenia
   LOOP
      IF r.z_konta = konto THEN wynik = wynik - r.kwota;
      ELSIF r.na_konto = konto THEN wynik = wynik + r.kwota;
      END IF;
      IF wynik < 0.00 THEN RAISE EXCEPTION 'Wykryto ujemny bilans konta';
      END IF;
   END LOOP;
   RETURN wynik;
 END;
$$ language plpgsql;


CREATE FUNCTION historia_konta(konto numeric(11))
  RETURNS TABLE (
    data timestamp,
    stan numeric(11, 2)) AS $$
     SELECT data_zlecenia as data,
                  stan_konta(konto, data_zlecenia) as stan
                  FROM transakcje t
                  WHERE na_konto = konto OR z_konta = konto
                  ORDER BY data;
$$ language 'sql';
----

--ZAD12
CREATE OR REPLACE FUNCTION stan_konta(konto numeric, czas timestamp) RETURNS numeric(11, 2) AS
$$
DECLARE
wynik numeric(11, 2);
r transakcje%rowtype;
 BEGIN
   wynik = 0.00;
   FOR r IN
      SELECT * FROM transakcje WHERE data_zlecenia <= czas ORDER BY data_zlecenia
   LOOP
      IF r.z_konta = konto THEN wynik = wynik - r.kwota;
      ELSIF r.na_konto = konto THEN wynik = wynik + r.kwota;
      END IF;
      IF wynik < 0.00 THEN RAISE EXCEPTION 'Wykryto ujemny bilans konta';
      END IF;
   END LOOP;
   RETURN wynik;
 END;
$$ language plpgsql;
----
--ZAD10b
create or replace function silnia(n numeric)
returns numeric as $$
 begin
  IF n = 0 THEN RETURN 1;
  ELSIF n = 1 THEN RETURN 1;
  ELSE RETURN n * silnia(n - 1);
  END IF;
 END;
$$ language plpgsql;
----
--ZAD10a
CREATE FUNCTION Silnia(n numeric)
RETURNS numeric as $$
 DECLARE
  wynik numeric;
 BEGIN
  wynik := 1;
  FOR i IN 1 .. n LOOP
   wynik := wynik * i;
  END LOOP;
  RETURN wynik;
 END;
$$ language plpgsql;
----
--ZAD9
CREATE FUNCTION bilans_kont() RETURNS TABLE(konto numeric(11), suma_wplat numeric(11, 2), suma_wyplat numeric(11, 2))
    AS $$ SELECT nr_konta,
    (select (CASE
        WHEN sum(kwota) is null THEN 0.00
      else sum(kwota)
      END)
    from transakcje t
    where k.nr_konta = t.na_konto),
    (select (CASE
        WHEN sum(kwota) is null THEN 0.00
      else sum(kwota)
      END)
    from transakcje t
    where k.nr_konta = t.z_konta)
    FROM konta k; $$
    LANGUAGE SQL;

SELECT konto, suma_wplat - suma_wyplat AS bilans
FROM bilans_kont();
----
--ZAD8
CREATE FUNCTION bilans_kont() RETURNS TABLE(konto numeric(11), suma_wplat numeric(11, 2), suma_wyplat numeric(11, 2))
    AS $$ SELECT nr_konta,
    (select (CASE
        WHEN sum(kwota) is null THEN 0.00
      else sum(kwota)
      END)
    from transakcje t
    where k.nr_konta = t.na_konto),
    (select (CASE
        WHEN sum(kwota) is null THEN 0.00
      else sum(kwota)
      END)
    from transakcje t
    where k.nr_konta = t.z_konta)
    FROM konta k; $$
    LANGUAGE SQL;
----

--ZAD7
create or replace function oblicz_koszt(numeric(11,2))
returns numeric(11,2) as
$$
declare
    kwota ALIAS FOR $1;
begin
    return round(kwota*(0.02), 2);
end;
$$
language plpgsql;

select oblicz_koszt(kwota) from transakcje;
----
--ZAD6
create or replace function oblicz_koszt(numeric(11,2))
returns numeric(11,2) as
$$
declare
    kwota ALIAS FOR $1;
begin
    return round(kwota*(0.02), 2);
end;
$$
language plpgsql;
----
--ZAD5
 CREATE OR REPLACE VIEW wplaty_wyplaty("Konto", "Ilosc wyplat", "Ilosc wplat") AS
 SELECT nr_konta,
(SELECT COUNT(z_konta)
 FROM transakcje
 WHERE z_konta = p.nr_konta),
(SELECT COUNT(na_konto)
 FROM transakcje
 WHERE na_konto= p.nr_konta)
 FROM konta p
 GROUP BY nr_konta;

 SELECT * FROM wplaty_wyplaty;

 INSERT INTO transakcje(id, na_konto, kwota)
 VALUES (200, 1004, 500.00);

 SELECT * FROM wplaty_wyplaty;

 DROP VIEW wplaty_wyplaty;
----

--ZAD4
 CREATE OR REPLACE VIEW wplaty_wyplaty("Konto", "Ilosc wyplat", "Ilosc wplat") AS
SELECT nr_konta,
 (SELECT COUNT(z_konta)
 FROM transakcje
 WHERE z_konta = p.nr_konta),
(SELECT COUNT(na_konto)
 FROM transakcje
 WHERE na_konto= p.nr_konta)
 FROM konta p
 GROUP BY nr_konta;
----
--ZAD3
CREATE UNIQUE INDEX ON transakcje(z_konta, na_konto, data_zlecenia);
----

--ZAD2
CREATE SEQUENCE seq START 10 MAXVALUE 99 INCREMENT BY 1;
INSERT INTO klienci (id, imie, nazwisko)
VALUES (nextval('seq'), 'Anna', 'Nowak');
INSERT INTO klienci (id, imie, nazwisko)
VALUES (nextval('seq'), 'Jan', 'Kowalski');
----
--ZAD1
CREATE SEQUENCE seq START 10 MAXVALUE 99 INCREMENT BY 1;
----
