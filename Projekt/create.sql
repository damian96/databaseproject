-- WIOSKA OLIMPIJSKA

CREATE TABLE IF NOT EXISTS wioska_olimpijska ( 
	id_budynku           integer  NOT NULL,
	nazwa_budynku        text  NOT NULL,
	ocena_hotelu         integer  NOT NULL,
	CONSTRAINT pk_wioska_olimpijska PRIMARY KEY ( id_budynku )
 );
 
-- NARODY 

CREATE TABLE IF NOT EXISTS narody ( 
	id_kraj              integer  NOT NULL,
	kraj                 text  NOT NULL,
	budynek              integer  NOT NULL,
	CONSTRAINT idx_narody PRIMARY KEY ( id_kraj ),
	CONSTRAINT pk_narody UNIQUE ( kraj )
	--CONSTRAINT pk_narody_0 UNIQUE ( budynek ) 
 );

ALTER TABLE narody ADD CONSTRAINT fk_narody_wioska_olimpijska FOREIGN KEY ( budynek ) REFERENCES wioska_olimpijska( id_budynku );

-- STREFY

CREATE TABLE IF NOT EXISTS strefy ( 
	id_strefy            integer  NOT NULL,
	nazwa                text  NOT NULL,
	miasto               text NOT NULL,
	CONSTRAINT pk_strefy PRIMARY KEY ( id_strefy )
 );

-- ARENY

CREATE TABLE IF NOT EXISTS areny ( 
	nr_areny             integer  NOT NULL,
	nazwa                text  NOT NULL,
	rok_otwarcia         text  NOT NULL,
	pojemność            integer  NOT NULL,
	strefa               integer  NOT NULL,
	CONSTRAINT pk_areny_0 UNIQUE ( nazwa ) ,
	CONSTRAINT pk_areny PRIMARY KEY ( nr_areny )
 );


ALTER TABLE areny ADD CONSTRAINT fk_areny_strefy FOREIGN KEY ( strefa ) REFERENCES strefy( id_strefy );

-- DYSCYPLINY

CREATE TABLE IF NOT EXISTS dyscypliny ( 
	id_konkurencji       integer  NOT NULL,
	konkurencja          text  NOT NULL,
	arena                integer  NOT NULL,
	typ                  char(1)  NOT NULL,
	CONSTRAINT pk_dyscypliny PRIMARY KEY ( id_konkurencji ),
	CONSTRAINT pk_konkurencje_indywidualne UNIQUE ( konkurencja ) 
 );


ALTER TABLE dyscypliny ADD CONSTRAINT fk_dyscypliny_indywidualne FOREIGN KEY ( arena ) REFERENCES areny( nr_areny );


-- ZESPOLY

CREATE TABLE IF NOT EXISTS zespoly ( 
	id_zespolu           integer  NOT NULL,
	kraj                 integer  NOT NULL,
	dyscyplina           integer  NOT NULL,
	płeć                 char(1)  NOT NULL,
	typ_rozgrywki        char(1)  NOT NULL,
	CONSTRAINT pk_zespoly PRIMARY KEY ( id_zespolu )
 );

ALTER TABLE zespoly ADD CONSTRAINT fkey_zespoly_narody FOREIGN KEY ( kraj ) REFERENCES narody( id_kraj );

ALTER TABLE zespoly ADD CONSTRAINT fk_zespoly_dyscypliny FOREIGN KEY ( dyscyplina ) REFERENCES dyscypliny( id_konkurencji );

-- ZAWODNICY

CREATE TABLE IF NOT EXISTS zawodnicy ( 
	id_zawodnika         integer NOT NULL,
	imie                 text  NOT NULL,
	nazwisko             text  NOT NULL,
	płeć                 text  NOT NULL,
	wiek                 integer  NOT NULL,
	kraj                 integer  NOT NULL,
	wystepy_olimpiada    integer  NOT NULL,
	indywidualnie        char(1) NOT NULL, 
	zespol               integer not null,
	CONSTRAINT pk_zawodnicy PRIMARY KEY ( id_zawodnika )
 );


ALTER TABLE zawodnicy ADD CONSTRAINT fk_zawodnicy_narody FOREIGN KEY ( kraj ) REFERENCES narody( id_kraj );


-- SPIS ZGLOSZEN

CREATE TABLE IF NOT EXISTS spis_zgloszen ( 
	id_zgloszenia        integer  NOT NULL,
	id_zawodnik          integer  NOT NULL,
	dyscyplina           integer  NOT NULL,
	typ_rozgrywki        char(1) NOT NULL,
	CONSTRAINT pk_spis_zgloszen PRIMARY KEY ( id_zgloszenia )
 );


ALTER TABLE spis_zgloszen ADD CONSTRAINT fk_spis_zgloszen_zawodnicy FOREIGN KEY ( id_zawodnik ) REFERENCES zawodnicy( id_zawodnika );

ALTER TABLE spis_zgloszen ADD CONSTRAINT fk_spis_zgloszen_dyscypliny FOREIGN KEY ( dyscyplina ) REFERENCES dyscypliny( id_konkurencji );

-- WYNIKI DRUZ INNE 

CREATE TABLE IF NOT EXISTS wyniki_druz_inne ( 
	id_zesp              integer  NOT NULL,
	wynik                numeric(7,3)  NOT NULL,
	jednostka            char(1)  NOT NULL
 );

ALTER TABLE wyniki_druz_inne ADD CONSTRAINT fk_wyniki_druz_inne_zespoly FOREIGN KEY ( id_zesp ) REFERENCES zespoly( id_zespolu );


-- WYNIKI INDYWIDUALNE 

CREATE TABLE IF NOT EXISTS wyniki ( 
	id_wynik             integer  NOT NULL,
	id_zgloszenia        integer  NOT NULL,
	wynik                numeric(7,3)  ,
	jednostka_wyniku     char(1) NOT NULL,
	podwyniki            char(1)  NOT NULL,
	CONSTRAINT pk_wyniki PRIMARY KEY ( id_wynik )
 );

ALTER TABLE wyniki ADD CONSTRAINT fk_wyniki_spis_zgloszen FOREIGN KEY ( id_zgloszenia ) REFERENCES spis_zgloszen( id_zgloszenia );

-- PODWYNIKI IND 

CREATE TABLE IF NOT EXISTS podwyniki_ind ( 
	id                   integer  NOT NULL,
	id_wyn            integer  NOT NULL,
	podwynik             text  NOT NULL,
	CONSTRAINT pk_podwyniki_ind PRIMARY KEY ( id )
 );

ALTER TABLE podwyniki_ind ADD CONSTRAINT fk_podwyniki_ind_wyniki FOREIGN KEY ( id_wyn) REFERENCES wyniki( id_wynik );


-- WYNIKI ZESPOŁÓW 

CREATE TABLE IF NOT EXISTS wyniki_ind_mecze ( 
	id            integer  NOT NULL,
	id_zglosz_1             integer  NOT NULL,
	id_zglosz_2             integer  NOT NULL,
	wynik             integer  NOT NULL,
	CONSTRAINT pk_wyniki_mecze PRIMARY KEY (id)
 );

ALTER TABLE wyniki_ind_mecze ADD CONSTRAINT fk_wyniki_mecze_zgloszenia FOREIGN KEY ( id_zglosz_1 ) REFERENCES spis_zgloszen( id_zgloszenia );

ALTER TABLE wyniki_ind_mecze ADD CONSTRAINT fk_wyniki_mecze_zgloszenia_2 FOREIGN KEY ( id_zglosz_2 ) REFERENCES spis_zgloszen( id_zgloszenia );

-- PODWYNIKI

CREATE TABLE IF NOT EXISTS podwyniki2 (
 id_podwynik integer NOT NULL,
 id_wynik integer NOT NULL,
 podwynik text NOT NULL,
 CONSTRAINT pk_podwyniki2 PRIMARY KEY(id_podwynik)
);

ALTER TABLE podwyniki2 ADD CONSTRAINT 
fk_podwyniki2_ind_mecze FOREIGN KEY (id_wynik) 
REFERENCES wyniki_ind_mecze(id);

-- SEDZIOWIE

CREATE TABLE IF NOT EXISTS sędziowie ( 
	id_sedziego          integer  NOT NULL,
	imie                 text  NOT NULL,
	nazwisko             text  NOT NULL,
	kraj                 text  NOT NULL,
	dyscyplina           text  NOT NULL,
	CONSTRAINT pk_sędziowie PRIMARY KEY ( id_sedziego )
 );


ALTER TABLE sędziowie ADD CONSTRAINT fk_sędziowie FOREIGN KEY ( dyscyplina ) REFERENCES dyscypliny( konkurencja );

-- REKORDY 

CREATE TABLE IF NOT EXISTS rekordy ( 
	dyscyplina          integer NOT NULL,
	rekord_swiata        numeric(7, 3),
	rekord_olimpijski    numeric(7, 3),
	płeć char(1) NOT NULL,
	jednostka  char(1)
 );

ALTER TABLE rekordy ADD CONSTRAINT fk_rekordy_swiata FOREIGN KEY ( dyscyplina ) REFERENCES dyscypliny( id_konkurencji );


-- WYNIKI MECZÓW 

CREATE TABLE IF NOT EXISTS wyniki_mecze ( 
	id_meczu             integer  NOT NULL,
	zespol_1             integer  NOT NULL,
	wynik_1              integer  NOT NULL,
	zespol_2             integer  NOT NULL,
	wynik_2              integer  NOT NULL,
	CONSTRAINT pk_wyniki_mecze_2 PRIMARY KEY ( id_meczu )
 );

ALTER TABLE wyniki_mecze ADD CONSTRAINT fk_wyniki_mecze_zespoly FOREIGN KEY ( zespol_1 ) REFERENCES zespoly( id_zespolu );

ALTER TABLE wyniki_mecze ADD CONSTRAINT fk_wyniki_mecze_zespoly2 FOREIGN KEY ( zespol_2 ) REFERENCES zespoly( id_zespolu );


-- PODWYNIKI DRUZ 

CREATE TABLE IF NOT EXISTS podwyniki ( 
	id_podwynik          integer  NOT NULL,
	id_meczu             integer  NOT NULL,
	podwynik             text  NOT NULL,
	CONSTRAINT pk_podwyniki PRIMARY KEY ( id_podwynik )
 );

ALTER TABLE podwyniki ADD CONSTRAINT fk_podwyniki_wyniki_mecze FOREIGN KEY ( id_meczu ) REFERENCES wyniki_mecze( id_meczu );
