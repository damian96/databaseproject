--ZAD10
SELECT table_name
  FROM information_schema.tables
 WHERE table_schema='public'
   AND table_type='BASE TABLE';
----

--ZAD12
SELECT relname AS "nazwa tabeli",
pg_total_relation_size(relid) AS "rozmiar"
FROM pg_catalog.pg_statio_user_tables;
----
--ZAD11
CREATE FUNCTION remove_all() RETURNS void AS $$
DECLARE
r RECORD;
BEGIN
FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema())
  LOOP
        EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.tablename) || ' CASCADE';
  END LOOP;
END
$$
LANGUAGE plpgsql;

SELECT remove_all();
----
--ZAD6
CREATE OR REPLACE FUNCTION array_sort(anyarray)
RETURNS anyarray AS $$
SELECT ARRAY(SELECT unnest($1) ORDER BY 1)
$$ LANGUAGE sql;
----

--ZAD5
CREATE FUNCTION array_intersect(anyarray, anyarray)
  RETURNS anyarray
as $$
    SELECT ARRAY(
        SELECT UNNEST($1)
        INTERSECT
        SELECT UNNEST($2)
    );
$$ language sql;
----
--ZAD2
--SELECT string_to_array(tab::text,','::text) as wynik FROM tab;
SELECT translate( string_to_array(x.*::text,',')::text, '()', '')::text[] AS wynik
FROM tab AS x;
----
--ZAD1
CREATE FUNCTION cast_int(input varchar)
RETURNS int AS $$
DECLARE
int_value int DEFAULT NULL;
BEGIN
    BEGIN
        int_value := input::int;
    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
    END;
RETURN int_value;
END;
$$ language plpgsql;
----
