--ZAD19
select (to_char(data_zlozenia,'Month')) as "Miesiac",
count(id_zamowienia) as "Ilosc zamowien"
from zamowienia
group by "Miesiac"
order by "Miesiac", "Ilosc zamowien";
----
--ZAD18
select count(*)
from klienci
where email LIKE '%@gmail.com';
----
--ZAD17
select
(CASE
when nip is not NULL = true then 'Podany'
when nip is not NULL = false then 'Brak'
END) as "NIP", count(*) as "ilosc klientow"
from klienci
group by (nip is not NULL)
order by "NIP";
----
--ZAD16
select produkt, count(id_zamowienia) as counter
from produkty_zamowienia
group by produkt
order by counter DESC, produkt;
----
--ZAD15b
select array_agg(numer_telefonu order by numer_telefonu) AS "lista telefonów"
from klienci
where numer_telefonu is not null;
----
--ZAD15a
select array_agg(numer_telefonu) AS "lista telefonów" from klienci where numer_telefonu is not null;
----
--ZAD14
select count(*)
from produkty
where kategoria = 'Artykuły papiernicze'
group by kategoria;
----
--ZAD13
select kategoria,
CASE
when round(variance(cena_netto),2)!=0 then round(variance(cena_netto),2)
else 0.00
END AS wariancja,
CASE
when round(stddev(cena_netto),2)!=0 then round(stddev(cena_netto),2)
else 0.00
END AS "standardowe odchylenie"
from produkty
group by kategoria
order by kategoria, wariancja, "standardowe odchylenie";
----
--ZAD12
select kategoria
from produkty
where vat=8
group by kategoria
having count(rabat)=count(*);
----
--ZAD11
select kategoria
from produkty
group by kategoria
having count(rabat)=count(*);
----
--ZAD10
select id_klienta
from zamowienia
group by id_klienta
having count(*) >1
order by id_klienta;
----
--ZAD9
select
CASE
when count(*)=count(distinct adres_dostawy) then 'TAK'
else 'NIE'
END
from zamowienia;
----
--ZAD8
select
CASE WHEN MAX(cena_netto)>=300 THEN sum(cena_netto)
            ELSE null
       END
from produkty;
----
--ZAD7
SELECT COUNT(CASE WHEN waga<1000 THEN '' END) FROM produkty;
----
--ZAD6
select kategoria, count(CASE WHEN vat!=18 THEN '' END) AS "ilosc produktow"
from produkty
group by kategoria
order by kategoria, "ilosc produktow";
----
--ZAD5
SELECT kategoria, MIN(cena_netto) AS min, MAX(cena_netto) AS max, MAX(cena_netto)-MIN(cena_netto) AS subtract
FROM produkty
GROUP BY kategoria
ORDER BY kategoria, min, max, subtract;
----
--ZAD4
SELECT kategoria ,ROUND(AVG(waga),2) AS "średnia waga"
FROM produkty
GROUP BY kategoria
HAVING count(kategoria) >=2
ORDER BY "średnia waga" DESC, kategoria;
----
--ZAD3
SELECT PRODUKT
FROM PRODUKTY_ZAMOWIENIA
GROUP BY PRODUKT
HAVING SUM(ILOSC)>=50
ORDER BY PRODUKT;
----
--ZAD2
SELECT ID_ZAMOWIENIA, SUM(ILOSC) AS SUMA
FROM PRODUKTY_ZAMOWIENIA
GROUP BY ID_ZAMOWIENIA
ORDER BY ID_ZAMOWIENIA;
----
--ZAD1
SELECT MIN(ROUND(CENA_NETTO*(1+(VAT/100)),2)) AS min, MAX(ROUND(CENA_NETTO*(1+(VAT/100)),2)) AS max
FROM PRODUKTY;
----
