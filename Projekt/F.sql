--ZAD18
CREATE TABLE pracownicy_zespoly (nazwisko, etat,
roczna_placa, zespol, adres_pracy) AS
SELECT nazwisko, etat, (12*placa_pod) AS roczna_placa, zespoly.nazwa as zespol, adres as adres_pracy
FROM pracownicy join zespoly using(id_zesp);
----

ID_PRAC NUMERIC(4) CONSTRAINT PK_PRAC PRIMARY KEY,
      NAZWISKO CHARACTER VARYING(15),
      IMIE CHARACTER VARYING(15),
      ETAT CHARACTER VARYING(15) CONSTRAINT FK_ETAT REFERENCES ETATY(NAZWA),
      ID_SZEFA NUMERIC(4) CONSTRAINT FK_ID_SZEFA REFERENCES PRACOWNICY(ID_PRAC),
      ZATRUDNIONY DATE,
      PLACA_POD NUMERIC(6,2) CONSTRAINT MIN_PLACA_POD CHECK(PLACA_POD>100),
      PLACA_DOD NUMERIC(6,2),
      ID_ZESP NUMERIC(2) CONSTRAINT FK_ID_ZESP REFERENCES ZESPOLY(ID_ZESP)

--ZAD17
ALTER TABLE pracownicy DROP COLUMN imie;
----
--ZAD15
CREATE TABLE if not exists projekty(
id_projektu numeric(4) PRIMARY KEY,
opis_projektu char(20) UNIQUE NOT NULL,
data_rozpoczecia date default current_date,
data_zakonczenia date check(data_zakonczenia > data_rozpoczecia),
fundusz numeric(7,2)
);

ALTER TABLE projekty ALTER COLUMN opis_projektu TYPE char(30);
----
--ZAD13
CREATE TABLE if not exists projekty(
id_projektu numeric(4) PRIMARY KEY,
opis_projektu char(20) UNIQUE NOT NULL,
data_rozpoczecia date default current_date,
data_zakonczenia date check(data_zakonczenia > data_rozpoczecia),
fundusz numeric(7,2)
);

CREATE TABLE if not exists przydzialy(
id_projektu numeric(4) REFERENCES projekty(id_projektu),
id_prac numeric(4) REFERENCES pracownicy(id_prac),
od date default current_date,
"do" date check(od < "do"),
stawka numeric(7,2),
rola char(20) check(rola IN ('KIERUJACY','ANALITYK','PROGRAMISTA')),
PRIMARY KEY(id_projektu, id_prac)
);

ALTER TABLE przydzialy ADD COLUMN godziny numeric;
----
--ZAD12
CREATE TABLE if not exists projekty(
id_projektu numeric(4) PRIMARY KEY,
opis_projektu char(20) UNIQUE NOT NULL,
data_rozpoczecia date default current_date,
data_zakonczenia date check(data_zakonczenia > data_rozpoczecia),
fundusz numeric(7,2)
);

CREATE TABLE if not exists przydzialy(
id_projektu numeric(4) REFERENCES projekty(id_projektu),
id_prac numeric(4) REFERENCES pracownicy(id_prac),
od date default current_date,
"do" date check(od < "do"),
stawka numeric(7,2),
rola char(20) check(rola IN ('KIERUJACY','ANALITYK','PROGRAMISTA')),
PRIMARY KEY(id_projektu, id_prac)
);
----
--ZAD11
CREATE TABLE if not exists projekty(
id_projektu numeric(4) PRIMARY KEY,
opis_projektu char(20) UNIQUE NOT NULL,
data_rozpoczecia date default current_date,
data_zakonczenia date check(data_zakonczenia > data_rozpoczecia),
fundusz numeric(7,2)
);
----
--ZAD10
CREATE TABLE if not exists zwierzeta(
gatunek CHARACTER VARYING(100) not null,
jajorodny char(1) not null,
liczba_konczyn numeric(2) not null,
data_odkrycia date not null
constraint cos CHECK(jajorodny='N' OR jajorodny='T')
);
ALTER TABLE zwierzeta RENAME TO gatunki;
DROP TABLE gatunki;
----

--ZAD9
CREATE TABLE if not exists plyty_cd(
kompozytor char(100) not null,
tytul_albumu char(100) not null,
CONSTRAINT un_ko_ty UNIQUE(kompozytor, tytul_albumu),
data_nagrania date,
data_wydania date,
czas_trwania INTERVAL DAY TO SECOND check(czas_trwania< INTERVAL '0 01:22:00' DAY TO SECOND),
check (data_nagrania < data_wydania)
);

ALTER TABLE plyty_cd DROP CONSTRAINT un_ko_ty;

ALTER TABLE plyty_cd ADD CONSTRAINT un_ko_ty PRIMARY KEY(kompozytor,tytul_albumu);

ALTER TABLE plyty_cd DROP CONSTRAINT un_ko_ty;

INSERT INTO plyty_cd(kompozytor, tytul_albumu,
data_nagrania, data_wydania, czas_trwania)
VALUES ('Example', 'Ex Ample', DATE '2000-12-31',
DATE '2001-01-01',INTERVAL '0 01:10:00' DAY TO SECOND);

INSERT INTO plyty_cd(kompozytor, tytul_albumu,
data_nagrania, data_wydania, czas_trwania)
VALUES ('Example', 'Ex Ample', DATE '2000-12-31',
DATE '2001-01-01',INTERVAL '0 01:10:00' DAY TO SECOND);
----

--ZAD8
CREATE TABLE if not exists plyty_cd(
kompozytor char(100) not null,
tytul_albumu char(100) not null,
CONSTRAINT un_ko_ty UNIQUE(kompozytor, tytul_albumu),
data_nagrania date,
data_wydania date,
czas_trwania INTERVAL DAY TO SECOND check(czas_trwania< INTERVAL '0 01:22:00' DAY TO SECOND),
check (data_nagrania < data_wydania)
);

ALTER TABLE plyty_cd DROP CONSTRAINT un_ko_ty;

ALTER TABLE plyty_cd ADD CONSTRAINT un_ko_ty PRIMARY KEY(kompozytor,tytul_albumu);
----

--ZAD7
CREATE TABLE szef_podwladny(szef, podwladny) AS
SELECT s.nazwisko, p.nazwisko
FROM pracownicy s JOIN pracownicy p ON (s.id_prac=p.id_szefa)
WHERE s.id_prac is not null AND p.id_szefa is not null;
----

--ZAD6
CREATE TABLE if not exists plyty_cd(
kompozytor char(100) not null,
tytul_albumu char(100) not null,
CONSTRAINT un_ko_ty UNIQUE(kompozytor, tytul_albumu),
data_nagrania date,
data_wydania date,
czas_trwania INTERVAL DAY TO SECOND check(czas_trwania< INTERVAL '0 01:22:00' DAY TO SECOND),
check (data_nagrania < data_wydania)
);
----

--ZAD5
CREATE TABLE if not exists pokoje(
numer_pokoju numeric(3),
id_zesp numeric(2) REFERENCES zespoly(id_zesp),
liczba_okien numeric(1),
PRIMARY KEY(numer_pokoju)
);
----

--ZAD4
CREATE TABLE if not exists ksiazki(
id_ksiazki numeric(10) primary key,
tytul varchar(100) not null,
autorzy varchar(100),
cena numeric(6,2),
data_wydania date
);
----
--ZAD3
CREATE TABLE if not exists uczelnie(
id_uczelni numeric(4) constraint pk_ucz primary key,
nazwa varchar(100),
adres varchar(100),
budzet numeric(10,2) not null,
zalozona date not null,
UNIQUE(nazwa)
);
----
--ZAD2
CREATE TABLE if not exists klienci(
pesel char(11) constraint pk_kl primary key,
adres varchar(100),
wiek numeric(2) not null,
wspolpraca_od date not null
);
----
--ZAD1
CREATE TABLE if not exists zwierzeta(
gatunek CHARACTER VARYING(100) not null,
jajorodny char(1) not null,
liczba_konczyn numeric(2) not null,
data_odkrycia date not null
constraint cos CHECK(jajorodny='N' OR jajorodny='T')
);
----
