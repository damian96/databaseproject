--ZAD9
ALTER TABLE etaty ADD COLUMN pensja_od numeric(8,2);
ALTER TABLE etaty ADD COLUMN pensja_do numeric(8,2);

UPDATE etaty SET
pensja_od=
(CASE
 WHEN widelki[1]<widelki[2] THEN widelki[1]
 ELSE widelki[2]
 END),
pensja_do=
(CASE
 WHEN widelki[1]<widelki[2] THEN widelki[2]
 ELSE widelki[1]
 END);

ALTER TABLE etaty ALTER COLUMN pensja_od SET NOT NULL;
ALTER TABLE etaty ALTER COLUMN pensja_do SET NOT NULL;
ALTER TABLE etaty DROP COLUMN widelki;

ALTER TABLE etaty ADD CONSTRAINT pensja check (pensja_od < pensja_do);

UPDATE pracownicy temp
SET pensja=
(SELECT(CASE
 WHEN pensja_od>temp.pensja THEN pensja_od
 ELSE temp.pensja
 END)
 FROM etaty
 WHERE etaty.etat=temp.etat);

INSERT INTO etaty(etat, pensja_od, pensja_do)
VALUES ('PROFESOR starszy', 3000.00, 8000.00),
('ADIUNKT starszy', 2510.00, 6000.00),
('ASYSTENT starszy', 1500.00, 4200.00),
('DOKTORANT starszy', 800.00, 2000.00),
('SEKRETARKA starszy', 1470.00, 3300.00),
('DYREKTOR starszy', 4280.00, 10200.00),
('STAŻYSTA starszy', 1500.00, 4200.00);

UPDATE pracownicy temp
SET etat=
(SELECT(CASE
 WHEN etaty.pensja_do<temp.pensja THEN temp.etat || ' starszy'
 ELSE temp.etat
 END)
 FROM etaty
 WHERE etaty.etat=temp.etat);

CREATE SEQUENCE seq START 10 INCREMENT BY 10;
ALTER TABLE etaty ADD COLUMN id_etatu INT;
UPDATE etaty
SET id_etatu = NEXTVAL('seq');

ALTER TABLE pracownicy
DROP CONSTRAINT pracownicy_etat_fkey;
ALTER TABLE etaty DROP CONSTRAINT etaty_pkey;
ALTER TABLE etaty ADD CONSTRAINT etaty_pkey PRIMARY KEY (id_etatu);

ALTER TABLE pracownicy ADD COLUMN etatt INT;
UPDATE pracownicy p
SET etatt = id_etatu
 FROM etaty
 WHERE p.etat = etaty.etat;
ALTER TABLE pracownicy ALTER COLUMN etatt SET NOT NULL;
ALTER TABLE pracownicy DROP COLUMN etat;
ALTER TABLE pracownicy RENAME etatt to etat;
ALTER TABLE pracownicy
ADD CONSTRAINT pracownicy_etat_fkey
FOREIGN KEY(etat)
REFERENCES etaty(id_etatu);

ALTER TABLE pracownicy ADD COLUMN dodatki numeric(8,2);
UPDATE pracownicy p SET
dodatki =
(CASE
WHEN pracownicy.pensja > pensja_do THEN (pracownicy.pensja - pensja_do)
 ELSE NULL
END)
FROM pracownicy join etaty on pracownicy.etat = id_etatu
WHERE p.pensja = pracownicy.pensja;

UPDATE pracownicy pr SET
pensja = (CASE
WHEN etaty.pensja_do < pracownicy.pensja THEN pensja_do
ELSE pracownicy.pensja
END)
FROM pracownicy join etaty on pracownicy.etat = etaty.id_etatu
WHERE pr.pensja = pracownicy.pensja;

SELECT imie, nazwisko, et.etat
FROM pracownicy pr CROSS JOIN etaty et
WHERE et.pensja_do =
(SELECT max(e.pensja_do)
FROM etaty e CROSS JOIN pracownicy p
WHERE e.pensja_do >= (COALESCE(pr.dodatki, 0)) + p.pensja
AND e.pensja_od <= (COALESCE(p.dodatki, 0)) + p.pensja
AND pr.id = p.id);
----
--ZAD3
ALTER TABLE rabaty_klientow
DROP CONSTRAINT fk_zam_kli,
DROP CONSTRAINT fk_zam_pol;
ALTER TABLE rabaty_klientow
ADD CONSTRAINT fk_zam_kli
FOREIGN KEY (id_klienta)
REFERENCES klienci(id_klienta)
ON DELETE CASCADE,
ADD CONSTRAINT fk_zam_pol
FOREIGN KEY (id_klienta)
REFERENCES klienci(id_klienta)
ON DELETE CASCADE;

ALTER TABLE zamowienia
DROP CONSTRAINT fk_zam_kli;
ALTER TABLE zamowienia
ADD CONSTRAINT fk_zam_kli
FOREIGN KEY (id_klienta)
REFERENCES klienci
ON DELETE SET NULL;
ALTER TABLE zamowienia
ALTER COLUMN id_klienta DROP NOT NULL;

DELETE FROM klienci;

ALTER TABLE zamowienia
DROP CONSTRAINT fk_zam_kli;
ALTER TABLE rabaty_klientow
DROP CONSTRAINT fk_zam_kli,
DROP CONSTRAINT fk_zam_pol;

DROP TABLE klienci CASCADE;
----
--ZAD2
CREATE TABLE historia_cen AS
SELECT kod_produktu,cena_netto FROM produkty;
ALTER TABLE historia_cen
ADD COLUMN data_wprowadzenia date;
UPDATE historia_cen
SET data_wprowadzenia = '2000-01-01';
ALTER TABLE historia_cen
ADD FOREIGN KEY (kod_produktu) REFERENCES produkty,
ALTER COLUMN cena_netto SET NOT NULL,
ADD PRIMARY KEY (kod_produktu,data_wprowadzenia);
ALTER TABLE produkty
DROP COLUMN cena_netto;
----
--ZAD1
CREATE TABLE if not exists kategorie
(
    id_kategoria SERIAL PRIMARY KEY NOT NULL,
    nazwa character varying(250) UNIQUE NOT NULL,
    vat numeric(3,1) NOT NULL
);

INSERT INTO kategorie (nazwa,vat) SELECT distinct kategoria, vat FROM produkty;

ALTER TABLE produkty ADD COLUMN id_kategoria int REFERENCES kategorie;

UPDATE produkty SET id_kategoria = (SELECT id_kategoria FROM kategorie WHERE nazwa = kategoria);

ALTER TABLE produkty DROP COLUMN kategoria;
ALTER TABLE produkty DROP COLUMN vat;
ALTER TABLE produkty ALTER id_kategoria SET not null;
----
